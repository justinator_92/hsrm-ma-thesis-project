import io
import tensorflow as tf
import numpy as np
import skimage.io

from generator import GrayBatchGenerator
from util import PDFWriter
from util import calculate_pixel_accuracy, convert_image_batch_to_one_hot, calculate_measure_for_batch, \
    create_positive_negative_image, convert_one_hot_to_gray_image

output_graph_path = "graph/Graph_Freezed.pb"
num_classes = 4
width = 128
height = 128
num_epochs = 1
batch_size = 10

image_gen = GrayBatchGenerator('./images/', batch_size, num_epochs, width, height, 3, num_classes)

with tf.Session() as session:
    # Load graph
    output_graph_def = tf.GraphDef()
    with open(output_graph_path, "rb") as f:
        output_graph_def.ParseFromString(f.read())
        session.graph.as_default()
        _ = tf.import_graph_def(output_graph_def, name="")

    output_node = session.graph.get_tensor_by_name("Model/probabilities:0")
    input_batch_images = session.graph.get_tensor_by_name("ALEX/input:0")
    input_keep_probability = session.graph.get_tensor_by_name("ALEX/input_keep_probability:0")

    measures = np.zeros(4)
    orig_images = []
    gt_mask = []
    output_images = []
    true_false = []

    counter = 0

    for img, mask in image_gen:
        # perform forward pass
        output = session.run(output_node, feed_dict={input_batch_images: img, input_keep_probability: 1})
        output = convert_image_batch_to_one_hot(output)

        # calculate measurements
        pixel_accuracy = calculate_pixel_accuracy(mask, output)
        acc = calculate_measure_for_batch('mean_accuracy', mask, output)
        iu = calculate_measure_for_batch('mean_iu', mask, output)
        freq_mean_iu = calculate_measure_for_batch('freq_mean_iu', mask, output)

        measures += np.array([pixel_accuracy, acc, iu, freq_mean_iu])

        test = convert_one_hot_to_gray_image(output[0])
        mask_test = convert_one_hot_to_gray_image(mask[0])

        orig_images.append(img[0].reshape(height, width))
        gt_mask.append(convert_one_hot_to_gray_image(mask[0]))
        output_images.append(test)
        true_false.append(create_positive_negative_image(mask_test, test))

        counter += 1
        if counter >= 20:
            break

# Write data to PDF File
pdf_writer = PDFWriter("result.pdf")
pdf_writer.create_measurements_table("Measurements", list(measures / counter),
                                     ["Pixel Accuracy", "Mean Accuracy", "Mean IU", "Frequency Mean IU"])

visualizations = []
for o, gt, out, tr in zip(orig_images, gt_mask, output_images, true_false):
    images = [io.BytesIO() for _ in range(4)]
    for c, i in enumerate([o, gt, out, tr]):
        skimage.io.imsave(images[c], i)
        images[c].seek(0)

    visualizations.append(tuple(images))

pdf_writer.create_segmentation_example(visualizations)
pdf_writer.build_file()
