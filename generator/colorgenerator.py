import numpy as np
import cv2
import skimage.transform

from generator import BatchGeneratorBase


class ColorBatchGenerator(BatchGeneratorBase):

    def __init__(self, image_path, batch_size, epochs, width, height, channels, classes, info=False):
        BatchGeneratorBase.__init__(self, image_path, batch_size, epochs, width, height, channels, classes, info)

    def _pre_process_labels(self, image):
        image = skimage.transform.resize(image, (self.height, self.width), preserve_range=True).astype(np.uint8)
        return image

    def _pre_process_masks(self, mask):
        mask = skimage.transform.resize(mask, (self.height, self.width), preserve_range=True).astype(np.uint8)
        c1 = (mask <= 43).astype(np.uint8)
        c2 = ((mask > 43) & (mask <= 128)).astype(np.uint8)
        c3 = ((mask > 128) & (mask <= 213)).astype(np.uint8)
        c4 = (mask > 213).astype(np.uint8)

        mask = np.stack([c1, c2, c3, c4], axis=2)
        return mask


class GrayBatchGenerator(BatchGeneratorBase):

    def __init__(self, image_path, batch_size, epochs, width, height, channels, classes, info=False):
        BatchGeneratorBase.__init__(self, image_path, batch_size, epochs, width, height, channels, classes, info)

    def _pre_process_labels(self, image):
        image = skimage.transform.resize(image, (self.height, self.width), preserve_range=True).astype(np.uint8)
        image = image.reshape(self.height, self.width, 1)
        return image

    def _pre_process_masks(self, mask):
        mask = skimage.transform.resize(mask, (self.height, self.width), preserve_range=True).astype(np.uint8)
        c1 = (mask <= 43).astype(np.uint8)
        c2 = ((mask > 43) & (mask <= 128)).astype(np.uint8)
        c3 = ((mask > 128) & (mask <= 213)).astype(np.uint8)
        c4 = (mask > 213).astype(np.uint8)

        mask = np.stack([c1, c2, c3, c4], axis=2)
        return mask

    def _read_image_from_disk(self, image_name):
        return cv2.imread(image_name, 0)

    def _read_mask_from_disk(self, mask_name):
        return cv2.imread(mask_name, 0)
