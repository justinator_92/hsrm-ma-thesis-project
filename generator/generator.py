import cv2
import numpy as np
import os
import random

from datetime import datetime


class BatchGeneratorBase(object):

    def __init__(self, image_path, batch_size, epochs, width, height, channels, classes, info=False):
        self.image_path = image_path
        self.batch_size = batch_size
        self.epochs = epochs
        self.width = width
        self.height = height
        self.channels = channels
        self.classes = classes
        self.info = info

        self.images = []  # [(image, mask), (image, mask), ...]
        self._initialize_generator()
        self.images_count = len(self.images)
        self.images_list_copy = list(self.images)

        self.progress = None
        self._cur_batch_idx = 0
        self._cur_epoch = 0
        self._cur_total_iteration = 0

        self.iter_per_epoch = self.images_count // self.batch_size
        self.total_iter = self.iter_per_epoch * self.epochs

        self.example_image = None

        print("{} GENERATOR loaded {} images from {}.".format(datetime.now(), len(self.images), self.image_path))
        print("{} GENERATOR delivers {} iterations per epoch. In total {}.".format(datetime.now(),
                                                                                   self.iter_per_epoch,
                                                                                   self.total_iter))

    def _initialize_generator(self):
        images = filter(lambda x: 'gray' in x, os.listdir(self.image_path))
        images = sorted(map(lambda x: os.path.join(self.image_path, x), images))

        masks = [x.replace('gray', 'mask') for x in images]
        self.images = list(zip(images, masks))

    def __iter__(self):
        """
        Method iterates over entire image_batch until it reaches max epochs and iterations
        """
        random.shuffle(self.images_list_copy)  # shuffle initial images list

        while True:
            # check if entire dataset per epoch was used
            if self._cur_batch_idx >= self.iter_per_epoch:
                self.images_list_copy = list(self.images)  # copy the list for current iteration
                random.shuffle(self.images_list_copy)
                self._cur_batch_idx = 0
                self._cur_epoch += 1

            if self._cur_epoch >= self.epochs:
                raise StopIteration

            images_names = self.images_list_copy[:self.batch_size]
            self.images_list_copy = self.images_list_copy[self.batch_size:]

            label, mask = self._create_image_batch(images_names)
            self._cur_total_iteration += 1

            progress = float("{0:.3f}".format(100 * self._cur_total_iteration / self.total_iter))
            print("{} GENERATOR \tProgress: {}%\t{}/{}".format(datetime.now(),
                                                               progress,
                                                               self._cur_total_iteration,
                                                               self.total_iter))
            yield label, mask
            self._cur_batch_idx += 1

    def iterate_nr_of_random_batches(self, nr_of_batches):
        """
        method iterates over a subset of random batches
        :param nr_of_batches: the number of desired random batches
        """
        assert (nr_of_batches * self.batch_size) <= self.images_count
        random_indices = random.sample(range(self.images_count), self.batch_size * nr_of_batches)

        for cur_batch in range(nr_of_batches):
            cur_indices = [random_indices.pop(0) for _ in range(self.batch_size)]
            images = [self.images[index] for index in cur_indices]

            label, mask = self._create_image_batch(images)
            yield label, mask

        assert(random_indices == [])  # make sure all indices were used
        raise StopIteration

    def _create_image_batch(self, image_data_names):
        images, masks = [], []

        # read images from disk
        for image_name, mask_name in image_data_names:
            image = self._read_image_from_disk(image_name)
            mask = self._read_mask_from_disk(mask_name)

            images.append(self._pre_process_labels(image))  # pre process image
            masks.append(self._pre_process_masks(mask))  # pre process mask

        return np.array(images), np.array(masks)

    def _read_image_from_disk(self, image_name):
        return cv2.imread(image_name)

    def _read_mask_from_disk(self, mask_name):
        return cv2.imread(mask_name)

    def _pre_process_labels(self, image):
        return image

    def _pre_process_masks(self, mask):
        return mask

    def get_cur_batch(self):
        return self._cur_batch_idx

    def get_cur_epoch(self):
        return self._cur_epoch

    def get_total_iteration(self):
        return self._cur_total_iteration

    def get_example_image(self):
        if self.example_image is None:
            file_name = self.images[random.randint(0, len(self.images) - 1)]  # pick a random image from dataset
            cv2.imwrite("img.png", cv2.imread(file_name[0], 0))
            cv2.imwrite("mask.png", cv2.imread(file_name[1], 0))
            self.example_image = self._create_image_batch([file_name])
        return self.example_image[0]
