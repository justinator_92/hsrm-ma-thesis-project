# FCN-AlexNet for Intensity Images #

Before running application, please download the .npy file under 

https://www.dropbox.com/s/oeptycoz92scjnv/bvlc_alexnet.npy?dl=0


and copy it to the alexnet/ directory (beside alexnet.py)

run main-alexnet.py for training
run eval-alexnet.py for evaluation

scripts will generate according output folders

