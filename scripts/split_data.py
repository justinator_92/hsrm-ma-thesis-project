import argparse
import os
import shutil


TRAIN = "_train"
TEST = "_test"
EVALUATE = "_eval"


def split_data(im_path):
    image_list = sorted(map(lambda x: os.path.join(im_path, x), filter(lambda f: "gray" in f, os.listdir(im_path))))
    masks_list = sorted(map(lambda x: os.path.join(im_path, x), filter(lambda f: "mask" in f, os.listdir(im_path))))
    print("images count:", len(image_list))
    print("masks count:", len(masks_list))
    assert(len(image_list) != 0 and len(image_list) == len(masks_list))

    base_path = im_path[:-1]

    for path in [TRAIN, TEST, EVALUATE]:
        path = base_path + path
        if os.path.exists(path):
            os.system("rm -rf " + path)
        os.mkdir(path)

    #  50 % Train Images
    #  25 % Test Images
    #  25 % Evaluation Images
    d = {0: TRAIN, 1: TRAIN, 2: TEST, 3: EVALUATE}

    counter = 0
    for image, mask in zip(image_list, masks_list):
        suffix = d[counter]
        copy_image_and_mask(image, mask, base_path + suffix)
        counter = (counter + 1) % 4

    print("Copy Data done.")


def copy_image_and_mask(src_img_path, src_mask_path, dst_path):
    shutil.copy2(src_img_path, dst_path)
    shutil.copy2(src_mask_path, dst_path)


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()

    # Single argument
    arg_parser.add_argument('--path', type=str, default='')
    # Parse args
    args = arg_parser.parse_args()
    print("Arguments: %s" % args)

    split_data(im_path=args.path)
