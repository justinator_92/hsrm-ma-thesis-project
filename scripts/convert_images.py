import cv2
import os
import numpy as np
import sys
import shutil


def convert_gray_images_10_bytes(src_path, dst_path):
    files = filter(lambda x: 'gray' in x, os.listdir(src_path))
    
    for file in files:
        # convert files
        img = cv2.imread(os.path.join(src_path, file), -1)
        img = (img / 1024 * 255).astype(np.uint8)
        # save files
        cv2.imwrite(os.path.join(dst_path, file.replace("tif", "png")), img)


def check_masks(src_path, nr_of_classes=4):
    files = filter(lambda x: 'mask' in x, os.listdir(src_path))
    files = list(map(lambda x: os.path.join(src_path, x), files))
    dic = {}

    for file in files[:200]:
        img = cv2.imread(file, 0)
        for h in range(img.shape[0]):
            for w in range(img.shape[1]):
                gray = img[h][w]
                if gray in dic:
                    dic[gray] += 1
                else:
                    dic[gray] = 1
    print(dic)


def copy_masks(src_path, dst_path):
    for file in filter(lambda x: 'mask' in x, os.listdir(src_path)):
        shutil.copy2(os.path.join(src_path, file), dst_path)


def compute_means(path):
    images = filter(lambda x: 'gray' in x, os.listdir(path))

    sums = 0
    total = 0

    for file in images:
        img = cv2.imread(os.path.join(path, file), 0)
        sums += np.sum(np.sum(img, axis=0), axis=0)
        total += img.shape[0] * img.shape[1]

    means = sums / total
    return means, total


def compute_standard_deviations(path, mean, total):
    images = filter(lambda x: 'gray' in x, os.listdir(path))
    deviation_square = 0

    for file in images:
        img = cv2.imread(os.path.join(path, file), 0)
        img = img.astype(np.float64)

        img -= mean
        squared_img = np.square(img)
        deviation_square += np.sum(np.sum(squared_img, axis=0), axis=0)

    variance = deviation_square / total
    standard_deviation = np.sqrt(variance)
    
    return standard_deviation
    

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("please run program as: python convert_images.py <opcode> <src_path> <dst_path>")
        sys.exit(-1)

    op_code = sys.argv[1]

    if op_code == "conv":
        src_path, dst_path = sys.argv[2], sys.argv[3]

        print("...convert gray images")
        convert_gray_images_10_bytes(src_path, dst_path)

        print("...copy masks")
        copy_masks(src_path, dst_path)
        
    elif op_code == "count":
        src_path = sys.argv[2]
        mean, total = compute_means(src_path)
        print("Mean:", mean)
        std_dev = compute_standard_deviations(src_path, mean, total)
        print("Dev:", std_dev)
        
    else:
        print("<opcode>: [conv, count]")
        sys.exit(-1)
