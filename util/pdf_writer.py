import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import datetime
import numpy as np
import re
import io
from reportlab.pdfgen import canvas as cnv
from reportlab.lib.units import mm
from reportlab.platypus import Table, TableStyle
from reportlab.lib import colors
from reportlab.rl_config import defaultPageSize
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.platypus import Image
from reportlab.lib.utils import ImageReader
from reportlab.lib.units import mm


def visualize_loss_function(losses):
    """ create image of loss function by given loss function coordinates """
    iterations, values = zip(*losses)
    plt.figure(figsize=(8, 5), dpi=100)

    plt.plot(iterations, values)
    margin_x = (max(iterations) - min(iterations)) / 7.0
    margin_y = (max(values) - min(values)) / 7.0
    plt.axis([min(iterations) - margin_x, max(iterations) + margin_x, min(values) - margin_y, max(values) + margin_y])
    plt.grid(True)

    plt.xlabel('Iterations')
    plt.ylabel('Loss')
    plt.tight_layout()

    image_mem = io.BytesIO()
    plt.savefig(image_mem, format='jpg')
    image_mem.seek(0)
    return image_mem


def to_rl_table_image(image, draw_height, draw_width):
    """ Converts an in memory image to a reportlab table image"""
    image_rl = Image(image)
    image_rl.drawHeight = draw_height * mm
    image_rl.drawWidth = draw_width * mm
    return image_rl


def to_rl_canvas_image(image):
    """ Converts an in memory image to a reportlab canvas image"""
    image_rl = ImageReader(image)
    return image_rl


class PDFWriter(object):

    def __init__(self, save_path):
        self.timestamp = datetime.datetime.now()
        self.canvas = cnv.Canvas(save_path)
        self.create_headline()

    def create_headline(self):
        page_width, page_height = defaultPageSize
        section_headline_y = page_height - 40
        # Draw Headline
        self.draw_centered_headline_text("CNN based segmentation evaluation", section_headline_y, 'Helvetica-Bold', 20)
        # Draw date
        self.draw_centered_headline_text(str(self.timestamp), section_headline_y - 30, 'Helvetica-Bold', 17)

    def create_metadata_section(self, metadata, train_data_dict):
        table_style = TableStyle([
            ('FONTSIZE', (0, 0), (0, -1), 14),
            ('FONTSIZE', (1, 0), (1, -1), 12),
            ('TEXTFONT', (0, 0), (-1, -1), 'Helvetica'),
        ])

        # Remove some unnecessary data
        del train_data_dict['progress_train_image']
        del train_data_dict['progress_test_image']
        del train_data_dict['train_losses']
        del train_data_dict['test_losses']
        del train_data_dict['test_total_images']

        # Add some test metadata
        train_data_dict['evaluation_duration'] = metadata['evaluation_duration']

        #  TODO: data[0] still valid because of TESTDATA
        for source, data in metadata['test_data'].items():
            train_data_dict[source] = data[0]

        # Draw metadata
        table_data = sorted([(k.replace('_', ' ').capitalize(), str(v)) for k, v in train_data_dict.items()])
        table = Table(table_data, colWidths=50 * mm, rowHeights=6 * mm)
        table.setStyle(table_style)
        w, h = table.wrapOn(self.canvas, 0, 0)
        y = 700
        table.drawOn(self.canvas, 20, y - h)

        # Headline metadata
        self.draw_centered_headline_text('Metadata', y + 20, 'Helvetica-Bold', 15)

    def create_measurements_table(self, headline, measurements, labels):
        table_style = TableStyle([
            ('FONTSIZE', (0, 0), (0, -1), 14),
            ('FONTSIZE', (1, 0), (1, -1), 12),
            ('TEXTFONT', (0, 0), (-1, -1), 'Helvetica'),
        ])

        # create list with tuples..  [("Pixel Accuracy", int(p)), ...]
        table_data = [(l, str(m)) for m, l in zip(measurements, labels)]
        table = Table(table_data, colWidths=50 * mm, rowHeights=6 * mm)
        table.setStyle(table_style)
        w, h = table.wrapOn(self.canvas, 0, 0)
        y_pos = 200
        table.drawOn(self.canvas, 20, y_pos - h)

        # Headline Measurements
        self.draw_centered_headline_text(headline, y_pos + 20, 'Helvetica-Bold', 15)

    def create_loss_functions(self, train_loss_function, test_loss_function):
        # create new page...
        self.canvas.showPage()

        # create images of loss functions
        train_loss_function = visualize_loss_function(train_loss_function)
        test_loss_function = visualize_loss_function(test_loss_function)

        page_width, page_height = defaultPageSize
        section_headline_y = page_height - 40

        self.draw_centered_headline_text("Loss functions", section_headline_y, 'Helvetica-Bold', 17)

        loss_function_img = to_rl_canvas_image(train_loss_function)
        width = page_width - 200
        height = (float(loss_function_img.getSize()[1]) / loss_function_img.getSize()[0]) * width
        y = page_height - 150 - height
        self.canvas.drawImage(loss_function_img, (page_width - width) / 2, y, width, height)

        self.draw_centered_headline_text("Train data", y + height + 20, 'Helvetica-Bold', 15)

        y = 50
        loss_function_img = to_rl_canvas_image(test_loss_function)
        self.canvas.drawImage(loss_function_img, (page_width - width) / 2, y, width, height)
        self.draw_centered_headline_text("Test data", y + height + 20, 'Helvetica-Bold', 15)
        self.canvas.showPage()

    def create_image_progress_history(self, image, labels, headline):
        page_width, page_height = defaultPageSize

        self.draw_centered_headline_text(headline, page_height - 40, 'Helvetica-Bold', 17)

        train_progress_img = to_rl_canvas_image(image)
        train_progress_labels = to_rl_canvas_image(labels)

        size = 120
        self.canvas.drawImage(train_progress_img, (page_width - size) / 2, page_height - size - 70, size, size)

        width = page_width - 50
        height = (train_progress_labels.getSize()[1] / train_progress_labels.getSize()[0]) * width

        self.canvas.drawImage(train_progress_labels, (page_width - width) / 2,
                              page_height - height - size - 90, width, height)
        self.canvas.showPage()

    def create_all_segmentation_examples(self, result_images, image_names, measurements_per_source, source_examples,
                                         measure_labels):
        page_width, page_height = defaultPageSize
        section_headline_y = page_height - 40

        for source in result_images.keys():
            source_splitted = map(lambda x: x.lower(), filter(None, re.split("([A-Z][^A-Z]*)", source)))
            source_repr = " ".join(source_splitted).capitalize()

            # Headline segmentation examples
            self.draw_centered_headline_text('Source evaluation', section_headline_y, 'Helvetica-Bold', 17)
            self.draw_centered_headline_text('"%s"' % source_repr, section_headline_y - 30, 'Helvetica-Bold', 15)

            examples = to_rl_canvas_image(source_examples[source])
            width = page_width - 200
            height = (float(examples.getSize()[1]) / examples.getSize()[0]) * width
            examples_pos = section_headline_y - 80 - height
            self.canvas.drawImage(examples, (page_width - width) / 2, examples_pos, width, height)

            self.create_measurements_table("Evaluation results", measurements_per_source[source], measure_labels)
            self.canvas.showPage()

            ri = result_images[source]
            names = image_names[source]

            self.create_segmentation_example(ri, names)

    def create_segmentation_example(self, result_images):
        self.canvas.showPage()
        page_width, page_height = defaultPageSize
        section_headline_y = page_height - 40

        self.draw_centered_headline_text('Segmentation examples', section_headline_y, 'Helvetica-Bold', 15)

        # Draw segmentation examples
        box_start = page_height - 110
        h = 0
        table_style = TableStyle([
            ('FONTSIZE', (0, 0), (-1, -1), 11),
            ('TEXTFONT', (0, 0), (-1, -1), 'Helvetica'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.grey),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.grey),
            ('ALIGNMENT', (0, 0), (-1, -1), 'CENTRE')
        ])

        for i, (image, label, output, pos_neg) in enumerate(result_images):
            # Convert to reportlab images
            image_rl = to_rl_table_image(image, 38, 38)
            label_rl = to_rl_table_image(label, 38, 38)
            output_rl = to_rl_table_image(output, 38, 38)
            pos_neg_rl = to_rl_table_image(pos_neg, 38, 38)

            # Create table headline
            text = self.canvas.beginText(60, box_start)
            text.setFont("Helvetica", 15)
            text.textLine("%s. %s" % (i + 1, "image"))
            self.canvas.drawText(text)

            # Create table
            table_data = [[image_rl, label_rl, output_rl, pos_neg_rl],
                          ['Test image', 'Ground truth label', 'Output label', 'Correct Predictions']]
            table = Table(table_data, colWidths=40 * mm, rowHeights=(40 * mm, 6 * mm))
            table.setStyle(table_style)

            # Draw table
            h = table.wrapOn(self.canvas, 0, 0)[1]
            table.drawOn(self.canvas, 60, (box_start - 140))

            # Compute table height and next table start position
            offset = h + 50
            box_start -= offset

            # Start a new page if not enough space if available
            if box_start < h + 10:
                self.canvas.showPage()
                box_start = page_height - 100

        if box_start >= h + 10:
            self.canvas.showPage()

    def create_conv_layer_outputs(self, output_visualizations, layer_visualization_images):
        page_width, page_height = defaultPageSize
        section_headline_y = page_height - 40

        # Headline segmentation examples
        self.draw_centered_headline_text('Outputs of different CNN layers', section_headline_y, 'Helvetica-Bold', 17)

        # Draw activations
        table_style = TableStyle([
            ('FONTSIZE', (0, 0), (-1, -1), 8),
            ('TEXTFONT', (0, 0), (-1, -1), 'Helvetica'),
            ('ALIGNMENT', (0, 0), (-1, -1), 'CENTRE'),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
        ])

        image_row_count = len(output_visualizations[0][0])

        start = section_headline_y - 50
        for i in range(0, len(output_visualizations), 10):
            part = output_visualizations[i:i + 10]

            table_data = [
                ['Original images'] + [to_rl_table_image(col, 20, 20) for col in layer_visualization_images]]
            table_data += [[row[1]] + [to_rl_table_image(col, 20, 40) for col in row[0]] for row in part]
            table = Table(table_data, colWidths=[50 * mm] + [50 * mm] * image_row_count, rowHeights=22 * mm)
            table.setStyle(table_style)
            width, height = table.wrapOn(self.canvas, 0, 0)
            table.drawOn(self.canvas, 10, start - height)

            self.canvas.showPage()
            start = page_height - 50

    def draw_centered_headline_text(self, headline, y, font, font_size):
        """ Draw a horizontally centered headline in the canvas """
        page_width = defaultPageSize[0]

        text_width = stringWidth(headline, font, font_size)
        text = self.canvas.beginText((page_width - text_width) / 2.0, y)
        text.setFont(font, font_size)
        text.textLine(headline)
        self.canvas.drawText(text)

    def build_file(self):
        self.canvas.save()
