import os
import numpy as np
import skimage.io
import io


def get_image_paths(path, prefix=None, suffix=None, ignore=None):
    """ Generates a list with all images found in given path. """
    images = []

    for root, subFolders, files in os.walk(path):
        for f in files:
            if ((prefix is None or f.startswith(prefix))
                    and (suffix is None or f.endswith(suffix))
                    and (ignore is None or ignore not in f)):
                image_path = os.path.join(root, f)
                images.append(image_path)

    return images


def get_image_file_names(path):
    """ Generates a list with all images respectively masks found in given path.
        returns [[image1_path, mask1_path], [image2_path, mask2_path], ...]
    """
    images = map(lambda x: os.path.join(path, x), filter(lambda y: 'gray' in y, os.listdir(path)))
    return [[x, x.replace('gray', 'mask')] for x in images]


def divide_na(a, b):
    """ Divides a by b. Returns N/A in case b == 0 """
    if b == 0:
        return "N/A"
    return a / b


def squeeze_label(label):
    """ Converts elements [1, 0] (background) to 0 and [0, 1] (foreground) to 1 """
    return label[:, :, 1]


def merge_images(images, per_row=5):
    full_image = None
    start = 0

    for i in range(per_row, len(images) + 1, per_row):
        full_image_row = np.concatenate(images[start:i], axis=1).astype(np.uint8)

        if i == per_row:
            full_image = full_image_row
        else:
            full_image = np.concatenate([full_image, full_image_row]).astype(np.uint8)

        start += per_row

    return full_image


def array_as_mem_image(array):
    array_mem = io.BytesIO()
    skimage.io.imsave(array_mem, array)
    array_mem.seek(0)

    return array_mem


def convert_one_hot_to_gray_image(probability_map):
    num_classes = probability_map.shape[2]

    depths = [probability_map[:, :, d] for d in range(num_classes)]  # split probability channels...
    gray_values = [int(g / (num_classes - 1) * 255) for g in range(num_classes)]
    probability_depths = []

    for depth_idx in range(num_classes):
        copy = list(depths)
        copy.pop(depth_idx)  # do not compare with same depth channel...

        # stack boolean maps on each other...
        depths_for_level = np.stack([depths[depth_idx] > copy[idx] for idx in range(num_classes - 1)], axis=2)
        depth_map = np.all(depths_for_level, axis=2)
        probability_depths.append(depth_map.astype(np.uint8) * gray_values[depth_idx])

    img = np.stack(probability_depths, axis=2)
    img = np.amax(img, axis=2)
    return img


def convert_image_batch_to_one_hot(output_batch):
    ret = [convert_softmax_to_one_hot(output_batch[i]) for i in range(output_batch.shape[0])]
    return np.stack(ret)


def convert_softmax_to_one_hot(probability_map):
    assert (len(probability_map.shape) == 3)
    num_classes = probability_map.shape[2]

    depths = [probability_map[:, :, d] for d in range(num_classes)]  # separate channels
    probability_depths = []

    for depth_idx in range(num_classes):
        copy = list(depths)
        copy.pop(depth_idx)

        # stack boolean maps on each other...
        depths_for_level = np.stack([depths[depth_idx] > copy[idx] for idx in range(num_classes - 1)], axis=2)
        depth_map = np.all(depths_for_level, axis=2)
        probability_depths.append(depth_map.astype(np.uint8))

    return np.stack(probability_depths, axis=2)


def calculate_measure_for_batch(measure, gt_batch, output_batch):
    measures = {"mean_accuracy": calculate_mean_pixel_accuracy,
                "mean_iu": calculate_mean_iu,
                "freq_mean_iu": calculate_freq_mean_iu}

    measure = measures[measure.lower()]

    n_batches = gt_batch.shape[0]
    batch_measures = []

    for batch_idx in range(n_batches):
        batch_measures.append(measure(gt_batch[batch_idx], output_batch[batch_idx]))

    return np.mean(batch_measures)


def calculate_mean_pixel_accuracy(gt_mask, output):
    n_cl = gt_mask.shape[2]
    accuracy = [0] * n_cl

    for i in range(n_cl):
        curr_gt_mask, curr_output = gt_mask[:, :, i], output[:, :, i]

        n_ii = np.sum(np.logical_and(curr_gt_mask, curr_output))
        t_i = np.sum(curr_gt_mask)

        if t_i != 0:
            accuracy[i] = n_ii / t_i

    return np.mean(accuracy)


def calculate_mean_iu(gt_mask, output):
    n_cl = gt_mask.shape[2]
    iu = [0] * n_cl

    for i in range(n_cl):
        curr_gt_mask, curr_output = gt_mask[:, :, i], output[:, :, i]

        if (np.sum(curr_gt_mask) == 0) or (np.sum(curr_output) == 0):
            continue

        n_ii = np.sum(np.logical_and(curr_gt_mask, curr_output))
        t_i = np.sum(curr_gt_mask)
        n_ij = np.sum(curr_output)

        div = (t_i + n_ij - n_ii)
        if div != 0:
            iu[i] = n_ii / div

    return np.mean(iu)


def calculate_freq_mean_iu(gt_mask, output):
    height, width, n_cl = gt_mask.shape
    freq_mean_iu = [0] * n_cl

    for i in range(n_cl):
        curr_gt_mask, curr_output = gt_mask[:, :, i], output[:, :, i]

        if np.sum(curr_gt_mask) == 0 or np.sum(curr_output) == 0:
            continue

        n_ii = np.sum(np.logical_and(curr_gt_mask, curr_output))
        t_i = np.sum(curr_gt_mask)
        n_ij = np.sum(curr_output)

        div = (t_i + n_ij - n_ii)
        if div != 0:
            freq_mean_iu[i] = (t_i * n_ii) / div

    sum_k_t_k = height * width  # pixel area, all pixels of image
    return np.sum(freq_mean_iu) / sum_k_t_k


def calculate_pixel_accuracy(gt_mask, output):
    pixel_accuracy = np.sum(np.logical_and(output, gt_mask)) / np.sum(gt_mask)
    return pixel_accuracy


def create_positive_negative_image(gt_mask, output):
    green = np.array([0, 255, 0])
    red = np.array([255, 0, 0])

    positive_mask = (gt_mask == output)

    w, h = gt_mask.shape
    color = np.zeros((w, h, 3))
    color[positive_mask == np.array([True])] = green
    color[positive_mask != np.array(True)] = red

    return color.astype(np.uint8)
