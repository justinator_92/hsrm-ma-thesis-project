import tensorflow as tf
import numpy as np

from math import ceil


class AlexNet(object):
    """Implementation of the AlexNet."""

    def __init__(self, x, keep_prob, num_classes, skip_layer, weights_path='alexnet/bvlc_alexnet.npy'):
        """Create the graph of the AlexNet model.
        Args:
            x: Placeholder for the input tensor.
            keep_prob: Dropout probability.
            num_classes: Number of classes in the dataset.
            skip_layer: List of names of the layer, that get trained from scratch
            weights_path: Complete path to the pretrained weight file, if it isn't in the same folder as this code
        """
        # Parse input arguments into class variables
        self.bottom = x
        self.num_classes = num_classes
        self.keep_prob = keep_prob
        self.skip_layers = skip_layer

        self.epsilon = tf.constant(value=1e-4)

        # Load the weights into memory
        self.weights_dict = np.load(weights_path, encoding='bytes').item()

        # mean and standard deviation
        self.mean = 38.878657094222355
        self.var = 34.13662345961089

        with tf.name_scope('Processing'):
            self.bottom = tf.clip_by_value(x - self.mean, 0, 255, name='clip')

        # 1st Layer: Conv (w ReLu) -> Lrn -> Pool
        self.conv1 = self._conv(self.bottom, k_size=11, num_outputs=96, stride=4, name='conv1')
        self.norm1 = self._lrn(self.conv1, 2, 1e-05, 0.75, name='norm1')
        self.pool1 = self._max_pool(self.norm1, k_size=3, stride=2, name='pool1')

        # 2nd Layer: Conv (w ReLu)  -> Lrn -> Pool with 2 groups
        self.conv2 = self._conv(self.pool1, k_size=5, num_outputs=256, stride=1, groups=2, name='conv2')
        self.norm2 = self._lrn(self.conv2, 2, 1e-05, 0.75, name='norm2')
        self.pool2 = self._max_pool(self.norm2, k_size=3, stride=2, name='pool2')

        # 3rd Layer: Conv (w ReLu)
        self.conv3 = self._conv(self.pool2, k_size=3, num_outputs=384, stride=1, name='conv3')

        # 4th Layer: Conv (w ReLu) splitted into two groups
        self.conv4 = self._conv(self.conv3, k_size=3, num_outputs=384, stride=1, groups=2, name='conv4')

        # 5th Layer: Conv (w ReLu) -> Pool splitted into two groups
        self.conv5 = self._conv(self.conv4, k_size=3, num_outputs=256, stride=1, groups=2, name='conv5')
        self.pool5 = self._max_pool(self.conv5, k_size=3, stride=2, name='pool5')

        # build FCN
        self.fc6 = self._fully_conv(self.pool5, [6, 6, 256, 4096], 'fc6')
        self.fc6_drop = tf.nn.dropout(self.fc6, self.keep_prob)

        self.fc7 = self._fully_conv(self.fc6_drop, [1, 1, 4096, 4096], 'fc7')
        self.fc7_drop = tf.nn.dropout(self.fc7, self.keep_prob)

        self.fc8 = self._fully_conv(self.fc7_drop, [1, 1, 4096, 1000], 'fc8')

        self.pred = tf.nn.softmax(self.fc8)

        self.upscore = self._deconv(self.fc8, tf.shape(self.bottom), k_size=64, stride=32, name="upscore_fc8",
                                    num_classes=self.num_classes)

        self.prob_up = tf.nn.softmax(self.upscore, name="probabilities")

        # self.logits = tf.reshape(self.upscore, (-1, num_classes))
        # self.softmax = tf.nn.softmax(self.logits + self.epsilon)
        #
        # input_shape = self.bottom.get_shape().as_list()
        # input_shape[0] = -1  # self.batchSize # Images in batch
        # input_shape[3] = num_classes
        #
        # self.pred_up = tf.reshape(self.softmax, input_shape, name="probabilities")

        # 6th Layer: Flatten -> FC (w ReLu) -> Dropout
        #flattened = tf.reshape(pool5, [-1, 6 * 6 * 256])
        #fc6 = fc(flattened, 6 * 6 * 256, 4096, name='fc6')
        #dropout6 = dropout(fc6, self.KEEP_PROB)

        # 7th Layer: FC (w ReLu) -> Dropout
        #fc7 = fc(dropout6, 4096, 4096, name='fc7')
        #dropout7 = dropout(fc7, self.KEEP_PROB)

        # 8th Layer: FC and return unscaled activations
        #self.fc8 = fc(dropout7, 4096, self.NUM_CLASSES, relu=False, name='fc8')

    
    
    
    def load_initial_weights(self, session):
        """Load weights from file into network.
        As the weights from http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/
        come as a dict of lists (e.g. weights['conv1'] is a list) and not as
        dict of dicts (e.g. weights['conv1'] is a dict with keys 'weights' &
        'biases') we need a special load function
        """
        # Loop over all layer names stored in the weights dict
        for op_name in self.weights_dict:

            # Check if layer should be trained from scratch
            if op_name not in self.skip_layers:

                with tf.variable_scope(op_name, reuse=True):

                    # Assign weights/biases to their corresponding tf variable
                    for data in self.weights_dict[op_name]:

                        # Biases
                        if len(data.shape) == 1:
                            var = tf.get_variable('biases', trainable=False)
                            session.run(var.assign(data))

                        # Weights
                        else:
                            # handle special weights
                            if op_name == 'conv1':
                                data = np.mean(data, axis=2).reshape(11, 11, 1, 96)
                            var = tf.get_variable('weights', trainable=False)
                            session.run(var.assign(data))
        
        
    
    

    def initialize_for_full_training(self, session):
        """Initializes weights and weight of the network.
        Biases are set to the constant '0', weights are initialized using Xavier Initialization (stdev=np.sqrt(1/<nodes_in>))
        """
        
        # traverse through all variables that are trainable
        for variable in tf.trainable_variables():
        
            # there is probably a better name to get the actual name of the variable
            op_code = variable.name.split("/")[-1].rsplit(':0', 1)[0]
            
            if op_code == 'weights':
                
                # initialize weights with xavier initialization
                no_nodes = int(variable.shape[0])*int(variable.shape[1])*int(variable.shape[2])                
                weights = tf.truncated_normal(shape=variable.shape, dtype=tf.float32, stddev=np.sqrt(1.0/no_nodes))
                session.run(variable.assign(weights))
            
            
            elif op_code == 'biases':
                
                # initialize it with constant 0
                biases = tf.constant(0, dtype=tf.float32, shape=variable.shape)
                session.run(variable.assign(biases))
            
            elif op_code == 'up_filter':
                print("up-filter is already initialized")
            
            else:
                print("Something is wrong with the op_name!")
            

                
                    


    def _conv(self, bottom, k_size, num_outputs, stride, name, padding='SAME', groups=1):
        """Create a convolution layer. Adapted from: https://github.com/ethereon/caffe-tensorflow """
        # Get number of input channels
        input_channels = int(bottom.get_shape()[-1])

        # Create lambda function for the convolution
        convolve = lambda i, k: tf.nn.conv2d(i, k, strides=[1, stride, stride, 1], padding=padding)

        with tf.variable_scope(name) as scope:
            # Create tf variables for the weights and biases of the conv layer
            weights = tf.get_variable('weights', shape=[k_size, k_size, input_channels / groups, num_outputs])
            biases = tf.get_variable('biases', shape=[num_outputs])

        if groups == 1:
            conv = convolve(bottom, weights)

        # In the cases of multiple groups, split inputs & weights and
        else:
            # Split input and weights and convolve them separately
            input_groups = tf.split(axis=3, num_or_size_splits=groups, value=bottom)
            weight_groups = tf.split(axis=3, num_or_size_splits=groups, value=weights)
            output_groups = [convolve(i, k) for i, k in zip(input_groups, weight_groups)]

            # Concat the convolved output together again
            conv = tf.concat(axis=3, values=output_groups)

        # Add biases
        bias = tf.reshape(tf.nn.bias_add(conv, biases), tf.shape(conv))

        # Apply relu function
        relu = tf.nn.relu(bias, name=scope.name)
        return relu

    def _deconv(self, x, shape, name, num_classes, k_size, stride=2, padding='SAME'):
        strides = [1, stride, stride, 1]
        print("strides", strides)

        with tf.variable_scope(name):
            in_features = x.get_shape()[3].value

            new_shape = [shape[0], shape[1], shape[2], num_classes]
            output_shape = tf.stack(new_shape)

            f_shape = [k_size, k_size, num_classes, in_features]

            weights = self._get_deconv_filter(f_shape)
            print("weights", weights.shape)
            deconv = tf.nn.conv2d_transpose(x, weights, output_shape, strides=strides, padding=padding, name=name)
            return deconv

    def _get_deconv_filter(self, f_shape):
        width = f_shape[0]
        height = f_shape[1]
        f = ceil(width / 2.0)
        c = (2 * f - 1 - f % 2) / (2.0 * f)
        bilinear = np.zeros([f_shape[0], f_shape[1]])
        for x in range(width):
            for y in range(height):
                value = (1 - abs(x / f - c)) * (1 - abs(y / f - c))
                bilinear[x, y] = value

        weights = np.zeros(f_shape)

        for i in range(f_shape[2]):
            weights[:, :, i, i] = bilinear

        init = tf.constant_initializer(value=weights, dtype=tf.float32)
        return tf.get_variable(name="up_filter", initializer=init, shape=weights.shape)

    def _fc_layer(self, x, num_in, num_out, name, relu=True):
        """Create a fully connected layer."""
        with tf.variable_scope(name) as scope:

            # Create tf variables for the weights and biases
            weights = tf.get_variable('weights', shape=[num_in, num_out], trainable=True)
            biases = tf.get_variable('biases', [num_out], trainable=True)

            # Matrix multiply weights and inputs and add bias
            act = tf.nn.xw_plus_b(x, weights, biases, name=scope.name)

        if relu:
            # Apply ReLu non linearity
            relu = tf.nn.relu(act)
            return relu
        else:
            return act

    def _max_pool(self, x, k_size, stride, name, padding='SAME'):
        """Create a max pooling layer."""
        return tf.nn.max_pool(x, ksize=[1, k_size, k_size, 1], strides=[1, stride, stride, 1], padding=padding, name=name)

    def _lrn(self, x, radius, alpha, beta, name, bias=1.0):
        """Create a local response normalization layer."""
        return tf.nn.local_response_normalization(x, depth_radius=radius, alpha=alpha, beta=beta, bias=bias, name=name)

    def _dropout(self, x, keep_prob):
        """Create a dropout layer."""
        return tf.nn.dropout(x, keep_prob)

    def _fully_conv(self, x, shape, name):
        with tf.variable_scope(name):

            if name == 'fc6':
                filt = self.get_fc_weight_reshape(name, shape)
            elif name == 'fc8':
                filt = self.get_fc_weight_reshape(name, shape, num_classes=self.num_classes)
            else:
                filt = self.get_fc_weight_reshape(name, shape)

            # get variable initializer...
            conv = tf.nn.conv2d(x, filt, [1, 1, 1, 1], padding='SAME')
            conv_biases = self.get_bias(name, num_classes=self.num_classes)
            bias = tf.nn.bias_add(conv, conv_biases)

            bias = tf.nn.relu(bias)
            return bias

    def get_fc_weight_reshape(self, name, shape, num_classes=None):
        print('Layer name: %s' % name)
        print('Layer shape: %s' % shape)
        weights = self.weights_dict[name][0]
        weights = weights.reshape(shape)

        if num_classes is not None:
            weights = self._summary_reshape(weights, shape, num_new=num_classes)

        init = tf.constant_initializer(value=weights, dtype=tf.float32)
        return tf.get_variable(name="weights", initializer=init, shape=shape)

    def _summary_reshape(self, fweight, shape, num_new):
        """ Produce weights for a reduced fully-connected layer.
        FC8 of VGG produces 1000 classes. Most semantic segmentation
        task require much less classes. This reshapes the original weights
        to be used in a fully-convolutional layer which produces num_new
        classes. To archive this the average (mean) of n adjanced classes is
        taken.
        Consider reordering fweight, to perserve semantic meaning of the
        weights.
        Args:
          fweight: original weights
          shape: shape of the desired fully-convolutional layer
          num_new: number of new classes
        Returns:
          Filter weights for `num_new` classes.
        """
        num_orig = shape[3]
        shape[3] = num_new
        assert (num_new < num_orig)
        n_averaged_elements = num_orig // num_new
        avg_fweight = np.zeros(shape)
        for i in range(0, num_orig, n_averaged_elements):
            start_idx = i
            end_idx = start_idx + n_averaged_elements
            avg_idx = start_idx // n_averaged_elements
            if avg_idx == num_new:
                break
            avg_fweight[:, :, :, avg_idx] = np.mean(
                fweight[:, :, :, start_idx:end_idx], axis=3)
        return avg_fweight

    def get_bias(self, name, num_classes=None):
        bias_wights = self.weights_dict[name][1]
        shape = self.weights_dict[name][1].shape

        if name == 'fc8':
            bias_wights = self._bias_reshape(bias_wights, shape[0], num_classes)
            shape = [num_classes]

        init = tf.constant_initializer(value=bias_wights, dtype=tf.float32)
        return tf.get_variable(name="biases", initializer=init, shape=shape)

    def _bias_reshape(self, bweight, num_orig, num_new):
        """ Build bias weights for filter produces with `_summary_reshape`
        """
        n_averaged_elements = num_orig // num_new
        avg_bweight = np.zeros(num_new)
        for i in range(0, num_orig, n_averaged_elements):
            start_idx = i
            end_idx = start_idx + n_averaged_elements
            avg_idx = start_idx // n_averaged_elements
            if avg_idx == num_new:
                break
            avg_bweight[avg_idx] = np.mean(bweight[start_idx:end_idx])
        return avg_bweight


def calculate_loss(logits, labels, num_classes):
    """Calculate the loss from the logits and the labels.

    Args:
      logits: tensor, float - [batch_size, width, height, num_classes].
          Use vgg_fcn.up as logits.
      labels: Labels tensor, int32 - [batch_size, width, height, num_classes].
          The ground truth of your data.
      head: numpy array - [num_classes]
          Weighting the loss of each class
          Optional: Prioritize some classes

    Returns:
      loss: Loss tensor of type float.
    """
    with tf.name_scope('loss'):
        logits = tf.reshape(logits, (-1, num_classes))
        epsilon = tf.constant(value=1e-4)
        logits = logits + epsilon
        labels = tf.to_float(tf.reshape(labels, (-1, num_classes)))

        softmax = tf.nn.softmax(logits)

        cross_entropy = -tf.reduce_sum(labels * tf.log(softmax), axis=[1])

        cross_entropy_mean = tf.reduce_mean(cross_entropy, name='xentropy_mean')
        tf.add_to_collection('losses', cross_entropy_mean)

        loss = tf.add_n(tf.get_collection('losses'), name='total_loss')

    return loss
