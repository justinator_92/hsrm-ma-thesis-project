import os
import tensorflow as tf
import cv2
import numpy as np
import subprocess

from alexnet import AlexNet
from alexnet import calculate_loss
from datetime import datetime
from generator import GrayBatchGenerator

# Learning params
learning_rate = 0.0001
num_epochs = 3
batch_size = 16

# Network params
dropout_rate = 0.5
num_classes = 4
#train_layers = ['fc8', 'fc7', 'fc6', 'upscore_fc8']

train_layers = ['conv1', 'conv2', 'conv3', 'conv4', 'conv5', 'fc6', 'fc7', 'fc8','upscore_fc8']


# How often we want to write the tf.summary data to disk
display_step = 10

# Path for tf.summary.FileWriter and to store model checkpoints
filewriter_path = "tmp/finetune_alexnet/tensorboard/"
checkpoint_path = "tmp/finetune_alexnet/checkpoints/"
test_images_path = "test_images/"
graph_path = "graph"

width = 128
height = 128

image_gen = GrayBatchGenerator('./images/', batch_size, num_epochs, width, height, 3, num_classes)

# Create parent path if it doesn't exist
if not os.path.exists(checkpoint_path):
    os.makedirs(checkpoint_path)
if not os.path.exists(filewriter_path):
    os.makedirs(filewriter_path)
if not os.path.exists(test_images_path):
    os.mkdir(test_images_path)
if not os.path.exists(graph_path):
    os.mkdir(graph_path)

with tf.variable_scope('ALEX'):
    # TF placeholder for graph input and output
    x = tf.placeholder(dtype=tf.float32, shape=[None, height, width, 1], name='input')
    y = tf.placeholder(dtype=tf.int32, shape=[None, height, width, num_classes], name='ground_truth')
    keep_prob = tf.placeholder(dtype=tf.float32, name='input_keep_probability')

with tf.name_scope("Model"):
    # Initialize model
    model = AlexNet(x, keep_prob, num_classes, train_layers)

img = image_gen.get_example_image()

# Link variable to model output
score = model.prob_up

# List of trainable variables of the layers we want to train
var_list = [v for v in tf.trainable_variables() if v.name.split('/')[0] in train_layers]

print ("\n\n", [t.name for t in tf.trainable_variables()])

# Op for calculating the loss
with tf.name_scope("Loss"):
    loss = calculate_loss(model.prob_up, y, num_classes)

with tf.name_scope('Train'):
    # Define Optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)

    # Op to calculate every variable gradient
    gradients = tf.gradients(loss, var_list)
    gradients = list(zip(gradients, var_list))
    # Op to update all variables according to their gradient
    applyGradients = optimizer.apply_gradients(grads_and_vars=gradients)

# Add gradients to summary
for gradient, var in gradients:
    tf.summary.histogram(var.name + '/gradient', gradient)

# Add the variables we train to the summary
for var in var_list:
    tf.summary.histogram(var.name, var)

# Add the loss to summary
tf.summary.scalar('cross_entropy', loss)

# Evaluation op: Accuracy of the model
with tf.name_scope("accuracy"):
    correct_pred = tf.equal(tf.argmax(score, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Add the accuracy to the summary
tf.summary.scalar('accuracy', accuracy)

# Merge all summaries together
merged_summary = tf.summary.merge_all()

# Start Tensorflow session
with tf.Session() as sess:

    # Initialize the FileWriter
    writer = tf.summary.FileWriter(filewriter_path)

    # Initialize an saver for store model checkpoints
    saver = tf.train.Saver()

    # Initialize all variables
    sess.run(tf.global_variables_initializer())

    # Add the model graph to TensorBoard
    writer.add_graph(sess.graph)

    # Load the pretrained weights into the non-trainable layer
    #model.load_initial_weights(sess)
    model.initialize_for_full_training(sess)
    
    print ("\n\n", [t.name for t in tf.trainable_variables()])


    print("{} Start training...".format(datetime.now()))
    print("{} Open Tensorboard at --logdir {}".format(datetime.now(), filewriter_path))

    # Loop over number of epochs
    for img_batch, label_batch in image_gen:

        # Perform Backpropagation
        sess.run(applyGradients, feed_dict={x: img_batch, y: label_batch, keep_prob: dropout_rate})

        # Generate summary with the current batch of data and write to file
        if image_gen.get_cur_batch() % display_step == 0:
            s, img = sess.run([merged_summary, model.prob_up], feed_dict={x: img_batch, y: label_batch, keep_prob: 1.})
            writer.add_summary(s, image_gen.get_total_iteration())

            for c in range(img.shape[3]):
                g_img = (img[0, :, :, c] * 255).astype(np.uint8)
                cv2.imwrite('test_images/' + str(image_gen.get_total_iteration()) + '-' + str(c) + '-test.png', g_img)

            # Validate the model on the entire validation set
            #print("{} Start validation".format(datetime.now()))
            test_acc = 0.
            test_count = 0

            for v_img_batch, v_label_batch in image_gen.iterate_nr_of_random_batches(1):
                acc = sess.run(accuracy, feed_dict={x: v_img_batch, y: v_label_batch, keep_prob: 1.0})
                test_acc += acc
                test_count += 1

            test_acc /= test_count
            print("{} Validation Accuracy = {:.4f}".format(datetime.now(), test_acc))
            #print("{} Saving checkpoint of model...".format(datetime.now()))

            # save checkpoint of the model
            # checkpoint_name = os.path.join(checkpoint_path, 'model_epoch' + str(image_gen.get_cur_epoch() + 1) + '.ckpt')
            save_path = saver.save(sess, "tmp/finetune_alexnet/checkpoints/" + "model.ckpt", global_step=image_gen.get_total_iteration())

            #print("{} Model checkpoint saved at {}".format(datetime.now(), "tmp/..."))

    # Write Graph to file
    print("Writing Graph to File")
    os.system("rm -rf " + "graph/")
    tf.train.write_graph(sess.graph_def, "graph", "Graph_CNN.pb", as_text=False)  # proto buffer binary format

    input_checkpoint_path = "tmp/finetune_alexnet/checkpoints/" + "model.ckpt" + "-" + str(image_gen.get_total_iteration())

    output_node_names = "Model/probabilities"
    output_graph_path = "graph/Graph_Freezed.pb"
    input_graph_name = "graph/Graph_CNN.pb"

    # TODO: Be careful, had to change this to python3, because python is running python 2.7
    cmd = "python3 freeze_graph.py --input_graph " + input_graph_name + \
          " --input_checkpoint " + input_checkpoint_path + " --output_graph " + output_graph_path + \
          " --output_node_names " + output_node_names + " --input_binary"

    process = subprocess.Popen(cmd, shell=True)
    process.wait()
